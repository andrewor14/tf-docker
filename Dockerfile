# Ubuntu 18.04 Python3 with CUDA 10.1
#  - Installs requirements.txt for tensorflow/models

FROM nvidia/cuda:10.1-base-ubuntu18.04 as base

ENV CUDNN_VERSION 7.6.2.24-1+cuda10.1
ENV NCCL_VERSION 2.4.7-1+cuda10.1
ENV CUBLAS_VERSION 10.1.0.105-1

# Pick up some TF dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        cuda-command-line-tools-10-1 \
        cuda-cufft-10-1 \
        cuda-curand-10-1 \
        cuda-cusolver-10-1 \
        cuda-cusparse-10-1 \
        libcublas-dev=$CUBLAS_VERSION \
        libcudnn7=$CUDNN_VERSION \
        libnccl2=$NCCL_VERSION \
        libnccl-dev=$NCCL_VERSION \
        libfreetype6-dev \
        libhdf5-serial-dev \
        libzmq3-dev \
        libpng-dev \
        pkg-config \
        software-properties-common \
        unzip \
        lsb-core \
        curl

# Cublas hack, see https://forums.developer.nvidia.com/t/cublas-for-10-1-is-missing/71015
RUN apt-get install -y libcublas10=$CUBLAS_VERSION --allow-downgrades
RUN ln -s /usr/lib/x86_64-linux-gnu/libcublas.so.10 /usr/local/cuda/lib64/libcublas.so.10
RUN ln -s /usr/lib/x86_64-linux-gnu/libcublasLt.so.10 /usr/local/cuda/lib64/libcublasLt.so.10

# For CUDA profiling, TensorFlow requires CUPTI.
ENV LD_LIBRARY_PATH /usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8

# Add google-cloud-sdk to the source list
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-$(lsb_release -c -s) main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

# Install extras needed by most models
RUN apt-get update && apt-get install -y --no-install-recommends \
      git \
      ca-certificates \
      wget \
      htop \
      zip \
      vim \
      ssh \
      net-tools \
      iputils-ping \
      netcat \
      telnet \
      openssh-server \
      google-cloud-sdk \
      cmake

# Install / update Python and Python3
RUN apt-get install -y --no-install-recommends \
      python3 \
      python3-dev \
      python3-pip \
      python3-setuptools \
      python3-venv

# Set up Python3 environment
RUN pip3 install --upgrade pip
# setuptools upgraded to fix install requirements from model garden.
RUN pip3 install wheel
RUN pip3 install --upgrade setuptools google-api-python-client pyyaml google-cloud google-cloud-bigquery mock
RUN pip3 install absl-py
RUN pip3 install -U scikit-learn
RUN pip3 install -U scipy
RUN pip3 install nvidia-ml-py3
RUN pip3 install psutil
RUN curl https://raw.githubusercontent.com/andrewor14/models/virtual/official/requirements.txt > /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt
RUN pip3 install gcloud google-cloud-storage
RUN pip3 freeze


# =============================
# |          VIRTUAL          |
# =============================

# Signal to downstream that we are in a container
ENV IN_DOCKER_CONTAINER true

# Set up working directory
RUN mkdir -p /root/dev
RUN mkdir -p /root/dev/logs
RUN mkdir -p /root/dev/train_data
RUN mkdir -p /root/dev/eval_data
RUN mkdir -p /root/dev/dataset
RUN mkdir -p /root/lib
RUN mkdir -p /root/.ssh

# Clone various repos
WORKDIR /root/dev
RUN git clone -b virtual https://github.com/andrewor14/models
RUN git clone -b virtual https://bitbucket.org/andrewor14/tf-docker
RUN git clone -b virtual https://bitbucket.org/andrewor14/tf-wheel
RUN git clone -b virtual https://bitbucket.org/andrewor14/tf-models-dataset
RUN git clone --recursive -b virtual https://github.com/andrewor14/horovod

# Install openmpi and mpi4py
WORKDIR /root/lib
RUN wget https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.1.tar.gz
RUN tar -vzxf openmpi-4.0.1.tar.gz
WORKDIR openmpi-4.0.1
RUN ./configure --with-cuda=/usr/local/cuda
RUN make all install
ENV MPI_HOME /usr/local
ENV LD_LIBRARY_PATH /usr/local/lib:$LD_LIBRARY_PATH
RUN pip3 install mpi4py

# Install tensorflow
WORKDIR /root/dev/tf-wheel
RUN pip3 install tensorflow*.whl

# Install horovod
WORKDIR /root/dev/horovod
ENV HOROVOD_GPU_OPERATIONS NCCL
RUN ./build_and_install.sh

# Fetch cifar10
WORKDIR /root/dev/dataset
RUN mkdir cifar10
WORKDIR cifar10
RUN wget https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz
RUN tar -vzxf cifar-10-binary.tar.gz

# Set up BERT and transformer data
WORKDIR /root/dev/dataset
RUN mkdir bert
RUN mkdir transformer
RUN cp /root/dev/tf-models-dataset/*tgz .
RUN tar -vzxf bert-glue.tgz
RUN tar -vzxf bert-pretraining.tgz
RUN tar -vzxf transformer.tgz
RUN mv bert-glue bert/finetuning
RUN mv bert-pretraining bert/pretraining
RUN gsutil cp -r gs://cloud-tpu-checkpoints/bert/keras_bert/uncased_L-24_H-1024_A-16 bert
RUN gsutil cp -r gs://cloud-tpu-checkpoints/bert/keras_bert/uncased_L-12_H-768_A-12 bert
RUN rm *tgz

# Other
EXPOSE 1-65535
WORKDIR /root
ENV MPI_HOST_FILE /root/container_hosts/hosts.txt
RUN ldconfig
ENTRYPOINT service ssh restart && bash

