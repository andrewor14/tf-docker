#!/usr/bin/env python3

# =============================================================
#  A helper script to enforce synchronization barrier across
#  the hosts specified in the host file. Each host must run
#  this script at the same time with the same exact host file.
# =============================================================

import socket
import sys
import time
import threading
import xmlrpc.server

PORT = 16060
RETRY_INTERVAL_SECONDS = 0.5

def main():
  args = sys.argv
  if len(args) != 2:
    print("Usage: sync_barrier.py [host_file]")
    sys.exit(1)

  # Gather expected hostnames
  host_file = args[1]
  with open(host_file) as f:
    hostnames = [l.strip() for l in f.readlines()]
  # If there is only one node, then there is no need to sync
  if len(hostnames) == 1:
    return
  print("===================================================")
  print("Starting synchronization barrier with %s processes" % len(hostnames))
  
  # Start a server to receive registration requests
  server = xmlrpc.server.SimpleXMLRPCServer(
    (socket.gethostname(), PORT), logRequests=False, allow_none=True)
  received_hostnames = set()
  def register_hostname(hostname):
    print("Received hostname '%s'" % hostname)
    if hostname not in hostnames:
      raise ValueError("Unexpected hostname '%s'" % hostname)
    received_hostnames.add(hostname)
    if received_hostnames == set(hostnames):
      print("Synchronization barrier reached")
      print("===================================================")
      threading.Thread(target=server.shutdown).start()
  server.register_function(register_hostname)
  threading.Thread(target=server.serve_forever).start()

  # Send registration requests to everyone
  for host in hostnames:
    client = xmlrpc.client.ServerProxy("http://%s:%s" % (host, PORT))
    connected = False
    while not connected:
      try:
        client.register_hostname(socket.gethostname())
        connected = True
      except (ConnectionRefusedError, OSError) as e:
        time.sleep(RETRY_INTERVAL_SECONDS)

if __name__ == "__main__":
  main()

