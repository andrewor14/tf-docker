#!/bin/bash

DISTRIBUTION=""
if [[ -n "$(cat /etc/os-release | grep ID=debian)" ]]; then
  DISTRIBUTION="debian"
elif [[ -n "$(cat /etc/os-release | grep ID=ubuntu)" ]]; then
  DISTRIBUTION="ubuntu"
else
  print "Error: unknown distribution"
  exit 1
fi

# Instructions from https://docs.docker.com/engine/install/debian/ or .../ubuntu/
sudo apt-get remove -y docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install -y\
  apt-transport-https\
  ca-certificates curl\
  gnupg-agent\
  software-properties-common
curl -fsSL "https://download.docker.com/linux/$DISTRIBUTION/gpg" | sudo apt-key add -
sudo add-apt-repository\
  "deb [arch=amd64] https://download.docker.com/linux/$DISTRIBUTION $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

