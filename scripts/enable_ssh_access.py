#!/usr/bin/env python3

# ======================================================================
#  A helper script to enable SSH access between all the nodes described
#  in the given host file.
#
#  This script serves the current host's public key (id_rsa.pub) and
#  fetches that of all other hosts in the host file. The public keys
#  collected are appended to the current host's authorized_keys file.
#  Each host must run this script at the same time with the same exact
#  host file. Port 14040 must be open.
# ======================================================================

import os
import socket
from subprocess import Popen, PIPE
import sys
import time
import threading
import xmlrpc.server

PORT = 14040
VERBOSE = True
PUBLIC_KEY_FILE_NAME = "id_rsa.pub"
AUTHORIZED_KEYS_FILE_NAME = "authorized_keys"
CONFIG_FILE_NAME = "config"
RETRY_INTERVAL_SECONDS = 1

def main():
  args = sys.argv
  if len(args) != 2 and len(args) != 3:
    print("Usage: enable_ssh_access.py [host_file] <[ssh_dir]>")
    sys.exit(1)
  host_file = args[1]
  ssh_dir = args[2] if len(args) == 3 else os.path.join(os.environ["HOME"], ".ssh")

  # If public key doesn't exist, generate it
  public_key_file = os.path.join(ssh_dir, PUBLIC_KEY_FILE_NAME)
  if not os.path.isfile(public_key_file):
    print("Public key file does not exist, creating it at %s" % public_key_file)
    process = Popen(["ssh-keygen", "-f", public_key_file.rstrip(".pub"), "-N", ""],\
      stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    print(stdout.decode("UTF-8"))
    print(stderr.decode("UTF_8"))

  # Start a server in the background to listen for public key requests
  service = SSHKeyService(public_key_file)
  server = xmlrpc.server.SimpleXMLRPCServer(
    (socket.gethostname(), PORT), logRequests=False, allow_none=True)
  server.register_introspection_functions()
  server.register_multicall_functions()
  server.register_instance(service)
  thread = threading.Thread(target=server.serve_forever)
  thread.daemon = True
  thread.start()
  print("Listening for requests on port %s" % PORT)

  # Fetch public keys from remote servers
  print("Collecting public keys")
  with open(host_file) as f:
    servers = [connect("%s:%s" % (host.strip("\n\r"), PORT)) for host in f.readlines()]
  public_keys = [server.get_public_key() for server in servers]

  # Merge the new public keys with existing keys in authorized_keys
  print("Collected %s public keys, writing to authorized_keys file" % len(public_keys))
  authorized_keys_file = os.path.join(ssh_dir, AUTHORIZED_KEYS_FILE_NAME)
  new_authorized_keys = []
  if os.path.isfile(authorized_keys_file):
    with open(authorized_keys_file) as f:
      new_authorized_keys = [key.strip("\n\r") for key in f.readlines()]
  for key in public_keys:
    if key not in new_authorized_keys:
      new_authorized_keys.append(key)
  with open(authorized_keys_file, "w") as f:
    for key in new_authorized_keys:
      f.write("%s\n" % key)

  # Disable strict host key checking
  print("Disabling strict host key checking")
  config_file = os.path.join(ssh_dir, CONFIG_FILE_NAME)
  if os.path.isfile(config_file):
    print("Warning: overwriting %s" % config_file)
  with open(config_file, "w") as f:
    f.write("Host *\n")
    f.write("  StrictHostKeyChecking no\n")

  # Wait until everyone has fetched our key
  print("Waiting for everyone to fetch our key")
  while service.num_requests < len(servers):
    time.sleep(RETRY_INTERVAL_SECONDS)
  print("All done!")

def connect(host_port):
  """
  Connect to the given host port and return the corresponding ServerProxy object.
  This method retries indefinitely until success.
  """
  if not host_port.startswith("http://"):
    host_port = "http://%s" % host_port
  if VERBOSE:
    print("Connecting to %s" % host_port)
  server = xmlrpc.client.ServerProxy(host_port)
  while True:
    try:
      # The connection is not complete until we can access the server's methods
      server.system.listMethods()
      if VERBOSE:
        print("Connected to %s!" % host_port)
      return server
    except (ConnectionRefusedError, OSError) as e:
      if VERBOSE:
        print("... connection to %s failed, trying again in %s second(s)"\
          % (host_port, RETRY_INTERVAL_SECONDS))
      time.sleep(RETRY_INTERVAL_SECONDS)
    except Exception as e:
      print("Unexpected error %s (%s)" % (e, type(e)))
      raise e

class SSHKeyService:
  """
  A service that serves the current user's public key.
  """
  def __init__(self, public_key_file):
    with open(public_key_file) as f:
      self.public_key = f.read().strip("\n\r")
    self.num_requests = 0

  def get_public_key(self):
    self.num_requests += 1
    return self.public_key

if __name__ == "__main__":
  main()

