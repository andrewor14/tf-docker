#!/bin/bash

# ================================================================
#  A helper script that pulls the latest updates in the following
#  repos, re-installing tensorflow and horovod if necessary.
# ================================================================

DOCKER_DIR="${DOCKER_DIR:=/root/dev/tf-docker}"
WHEEL_DIR="${WHEEL_DIR:=/root/dev/tf-wheel}"
MODELS_DIR="${MODELS_DIR:=/root/dev/models}"
HOROVOD_DIR="${HOROVOD_DIR:=/root/dev/horovod}"
TF_MODELS_DATASET_DIR="${TF_MODELS_DATASET_DIR:=/root/dev/tf-models-dataset}"
BERT_DATA_DIR="${BERT_DATA_DIR:=/root/dev/dataset/bert}"
BERT_BASE_NAME="uncased_L-12_H-768_A-12"

# Make sure the directories exist
if [[ ! -d "$DOCKER_DIR" ]]; then
  echo "ERROR: $DOCKER_DIR does not exist"
  exit 1
fi
if [[ ! -d "$MODELS_DIR" ]]; then
  echo "ERROR: $MODELS_DIR does not exist"
  exit 1
fi
if [[ ! -d "$HOROVOD_DIR" ]]; then
  echo "ERROR: $HOROVOD_DIR does not exist"
  exit 1
fi
if [[ ! -d "$TF_MODELS_DATASET_DIR" ]]; then
  echo "ERROR: $TF_MODELS_DATASET_DIR does not exist"
  exit 1
fi

# Pull each repo and re-install tensorflow and horovod if there are updates
cd "$MODELS_DIR"
git pull origin virtual
cd "$DOCKER_DIR"
git pull origin virtual
cd "$WHEEL_DIR"
WHEEL_UP_TO_DATE="$(git pull origin virtual | grep 'up to date')"
if [[ -z "$WHEEL_UP_TO_DATE" ]]; then
  git reset --hard origin/virtual
  TF_WHEEL="$(ls | grep tensorflow.*whl)"
  pip3 uninstall -y tensorflow tensorflow-gpu
  pip3 install "$TF_WHEEL"
fi
cd "$HOROVOD_DIR"
HOROVOD_UP_TO_DATE="$(git pull origin virtual | grep 'up to date')"
if [[ -z "$WHEEL_UP_TO_DATE" ]] || [[ -z "$HOROVOD_UP_TO_DATE" ]]; then
  ./build_and_install.sh
fi
cd "$TF_MODELS_DATASET_DIR"
DATASET_UP_TO_DATE="$(git pull origin virtual | grep 'up to date')"
if [[ -z "$DATASET_UP_TO_DATE" ]] || [[ ! -f "$BERT_DATA_DIR/finetuning/RTE_meta_data" ]]; then
  # TODO: pull other data too
  rm -rf bert-glue
  tar -vzxf bert-glue.tgz
  mv bert-glue/* "$BERT_DATA_DIR/finetuning"
fi

# If BERT base doesn't exist, pull it
if [[ ! -d "$BERT_DATA_DIR/$BERT_BASE_NAME" ]]; then
  gsutil cp -r gs://cloud-tpu-checkpoints/bert/keras_bert/"$BERT_BASE_NAME" "$BERT_DATA_DIR"
fi

echo "Everything up to date"

