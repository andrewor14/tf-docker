#!/bin/bash

# ===================================================================
#  A helper script to fetch logs from all the hosts specified in the
#  host file. This requires the caller to have SSH access to all the
#  other nodes in the host file.
# ===================================================================

if [[ "$#" != "1" ]] && [[ "$#" != "2" ]]; then
  echo "Usage: fetch_logs.sh [host_file] <[log_directory]>"
  exit 1
fi

# Check if host file exists
HOST_FILE="$1"
if [[ ! -f "$HOST_FILE" ]]; then
  echo "ERROR: Host file $HOST_FILE does not exist"
  exit 1
fi

# Check if log directory exists
LOG_DIR="$2"
if [[ -z "$LOG_DIR" ]]; then
  LOG_DIR="/root/dev/logs"
fi
if [[ ! -d "$LOG_DIR" ]]; then
  echo "ERROR: Log dir $LOG_DIR does not exist"
  exit 1
fi

# For each host, recursively all the contents in LOG_DIR to the same
# location on this host
while read HOST; do
  echo "Copying $LOG_DIR from $HOST"
  rsync -a --ignore-existing --info=progress2 "$HOST:$LOG_DIR/*" "$LOG_DIR"
done < "$HOST_FILE"

echo "Done, check $LOG_DIR."

