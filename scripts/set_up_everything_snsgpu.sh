#!/bin/bash

# =================================================================== #
#  Helper script to set up environment, only for the snsgpu machine.  #
# =================================================================== #

sudo ./build_overlay_network.sh /home/andrew/hosts.txt virtual-net

ALL_CONTAINERS="virtual1 virtual2"

for container in $ALL_CONTAINERS; do
  sudo docker rm -f "$container"
  sudo docker run -dit --network=virtual-net --runtime=nvidia -v /home/andrew/container_hosts:/root/container_hosts -v /home/andrew/container_logs:/root/dev/logs -v /home/andrew/Documents/dev/dataset/imagenet:/root/dev/dataset/imagenet -v /home/andrew/Documents/dev/dataset/transformer-new:/root/dev/dataset/transformer-new --name "$container" virtual
done

sudo ./get_container_hostnames.sh /home/andrew/hosts.txt /home/andrew/container_hosts

for container in $ALL_CONTAINERS; do
  sudo docker exec -it "$container" /bin/bash -c "echo \"export TF_FORCE_GPU_ALLOW_GROWTH=true\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"alias jm='cd /root/dev/models'\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"alias js='cd /root/dev/models/deploy'\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"alias jv='cd /root/dev/models/virtual'\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"alias jl='cd /root/dev/logs'\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"alias jd='cd /root/dev/tf-docker'\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"export PYTHONPATH='\\\$PYTHONPATH:/root/dev/models'\" >> /root/.bashrc"
  sudo docker exec -it "$container" /bin/bash -c "echo \"export USE_RANK_FOR_CVD='true'\" >> /root/.bashrc"
done

